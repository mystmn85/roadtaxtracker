import csv
import datetime as dt
import random, string
import sys
from datetime import date, datetime


"""
TODO:
    Add functions to export generated data directly to database
"""

def gen_numbers():
    return random.randint(0, 9)

def gen_letters():
    ## Generate a random letter
    gen_letter = random.choice(string.ascii_letters)

    return gen_letter

def gen_order(x):
    creating_format = []
    '''
    Choose one of three license plate formats:
        1) XXX 000 or 000 XXX
        2) XX 0000 
        3) 00 XXXX
        4) All Random
    '''
    switcher = {
        1: [1,1,1,0,0,0],
        2: [1,1,0,0,0,0],
        3: [0,0,1,1,1,1],
        4: [2,2,2,2,2,2]
    }
    return switcher.get(x,'error')

def car_plate():
    pc = list()
    i=0

    plate = gen_order(random.randint(1, 4))
    ## print(f'plate generate_car :: {plate}')
    
    for each in plate:    
        if each == 0:
            pc.append(gen_letters().lower())
            ##print(f' gen letters :: {pc}')
        elif each == 1:
            pc.append(gen_numbers())
            ##print(f' gen numbers :: {pc}')
        else:
            multiple_selection = [gen_numbers(),gen_letters().lower()]
            pc.append(random.choice(multiple_selection))
    return pc

def date_gen():
    """
    Generate random date ranging from today and one year later
    """
    rd = random.randrange(0, 365)
    rod = dt.timedelta(days=rd)
    return datetime.strftime(date.today() + rod, '%d.%m.%Y')


def generate(number, typeof=None):
    """
    args:
        number --> number of vehicles to generate.

        typeof --> None to generate both commerical and cars randomly
               --> "cars" for car only
               --> "goods" for commerical vehicle only

    """
    generate_type = None
    list_of_cars = []

    if typeof is None:
        plate_generators = [car_plate, goods_plate]
        random_int = random.randint(0, len(plate_generators)-1)
        generate_type = plate_generators[random_int]
    elif typeof == "cars":
        generate_type = car_plate
    else:
        generate_type = goods_plate

    for _ in range(number):
        new_license_plate = generate_type()
        list_of_cars.append(new_license_plate)

    return car_plate()


def csv_writer(entries, typeof=None):
    """
    write a list of random generated vehicle number and roadtax expiry date
    to a CSV file with filename as "roadtax.csv"

    args:
        refer to generate(number, typeof = None) for more info

    """
    with open('roadtax.csv', 'w+', newline='') as f:
        fieldnames = ['CarPlate', 'ExpiryDate']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        items = generate(entries, typeof)
        writer.writeheader()
        for item in items:
            writer.writerow(
                {'CarPlate': f"{item}", 'ExpiryDate': f"{date_gen()}"})


def database_upload(entries, typeof=None):
    """
    write a list of random generated vehicle number and roadtax expiry date
    to a SQlite3 database with filename as "roadtax_date.db" according to
    model.py

    args:
        refer to generate(number, typeof = None) for more info

    """
    c = Crud()
    items = generate(entries, typeof)
    for item in items:
        c.add_new(item, date_gen())


def main():
    """
    running as a script.
        --> no arg provided, auto generate 100 random vehicle numbers
        --> number of vehicle number provided, generate number with random
            type
        --> number of vehicle and type provided, generate number with that
            named type. refer to generate function for typeof keywords
    """
    if len(sys.argv) == 2:
        generate(int(sys.argv[1]))
        input(
            f"{sys.argv[1]} random vehicle generated. Press any key to continue..")
    elif len(sys.argv) > 2:
        generate(int(sys.argv[1]), sys.argv[2])
        input(
            f"{sys.argv[1]} random vehicle generated. Press any key to continue..")
    else:
        generate(100)
        csv_writer(100) # Create csv file while generating values
        # Create Entries in Database(roadtax_date) table('vehicle') With the help of model class
        database_upload(100) 
        input(f"{100} random vehicle generated. Press any key to continue..")


if __name__ == '__main__':
    main()
